# Config file, command line parse with config option transformation
---

## INI file reader usage
---
```cpp
    cini ini("./sample.ini");
    for(auto&& [name,section] : ini) {
    ...
    }
```

## Commandline argument parse example
---
```cpp
    ccmdline cmd;
    cmd.option("help", 'h', 0, "Print this help").
        option("version", 'v', 0, "Print version and exit and exit").
        option("threads", 't', 1, "Number of working threads\nUse value between 1 - num-cpu-cores*2","Multi-thread options","<N>").
        option("affinity", 'A', 1, "Thread core binding list\nExample: 1-3,6. Bind thread to 1,2,3,6 cpu cores", "Multi-thread options", "<List>").
        option("config", 'c', 1, "Configuration file location.\nDefault name is config.ini", "Main options", "<FILE>").
        option("verbose", 'V', 1, "Set verbose level log\nBetween 0 (disable log) - 7. Default level is 1", "Main options", "<0...7>").
        option("affinity", 'A', 1, "Thread core binding list\nExample: 1-3,6. Bind thread to 1,2,3,6 cpu cores", "Main optionss", "<List>");

    try {
        cmd(argc, argv);
    }
    catch (std::string invalid_option) {
        cmd.print();
    }
    if(cmd.is("help")) {
        cmd.print();
        exit(0);
    }
```

## Option value transformation example
---
```cpp
    coption::number _num(" 2456 ", 40); assert(_num == 2456);
    
    coption::bytes _B("56", 0); assert(_B == 56);
    coption::bytes _K("3K", 0); assert(_K == 3 * 1024);
    coption::bytes _M("7M", 0); assert(_M == 7 * 1024 * 1024);
    coption::bytes _G("2G", 0); assert(_G == (size_t)2 * 1024 * 1024 * 1024);

    coption::seconds _s("56", 0); assert(_s == 56);
    coption::seconds _m("3m 14", 0); assert(_m == (3 * 60 + 14));
    coption::seconds _h("2h11m3", 0); assert(_h == (2 * 3600 + 11 * 60 + 3));
    coption::seconds _d(" 25d 11h 10m 59", 0); assert(_d == (25 * 24 * 3600 + 11 * 3600 + 10 * 60 + 59));

    coption::string _str(" test string   ");

    coption::sequence _seq(" three, one, two, two,, four    "), _seq_copy{ _seq };

    coption::array _arr(" three, one, two, two,, four    "), _arr_copy{ _arr };

    coption::dsn _sftp(" sftp://user:password@domain.tld:20022/home/root/1.txt "), _sftp_copy{ _sftp };
    coption::dsn _http(" http://www.domain.tld:8080/home/root/ ");
    coption::dsn _https(" https://www.domain.tld");

    coption::dsn _host(" *:8080/api/1.txt   "), _host_copy{ _host };
    coption::dsn _host1(" :8080/api/  ");
    coption::dsn _host2(" hostname:8080");

    coption::dsn _path(" api/  ");
    coption::dsn _path1(" /1.txt  ");
    coption::dsn _path2(" /api/sqwdswe/");

    enum class test_f {none = 0, one = 1,two = 2,four = 4,five = 8 };

    coption::flag<test_f> _flags("two , five", { {"none",test_f::none},{"one",test_f::one},{"two",test_f::two},{"four",test_f::four},{"five",test_f::five} });

    bool is_one = _flags & test_f::one;
    bool is_five = _flags & test_f::five;

    _flags |= test_f::one;

    is_one = _flags & test_f::one;
```