#pragma once
#include <cstddef>
#include <string>
#include <string_view>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <deque>
#include <vector>
#include <climits>
#include <stdexcept>

class coption
{
public:
	class seconds {
	public:
		seconds(size_t defvalue = 0) : value{ defvalue } { ; }
		seconds(const std::string_view&, size_t defvalue = 0);
		seconds(const seconds& v) : value{ v.value } { ; }
		seconds(const seconds&&) = delete;
		seconds& operator = (const seconds& ) = delete;
		seconds& operator = (const seconds&& v) { value = v.value; return *this; }
		inline operator size_t () { return value; }
		inline bool empty() { return !value; }
		template<typename T>
		inline bool operator == (T v) { return (T)value == v; }
		template<typename T>
		inline bool operator != (T v) { return (T)value != v; }
		template<typename T>
		inline bool operator > (T v) { return (T)value > v; }
		template<typename T>
		inline bool operator >= (T v) { return (T)value >= v; }
		template<typename T>
		inline bool operator < (T v) { return (T)value < v; }
		template<typename T>
		inline bool operator <= (T v) { return (T)value <= v; }
	public:
		size_t value;
	};

	class bytes {
	public:
		bytes(size_t defvalue = 0) : value{ defvalue } { ; }
		bytes(const std::string_view&, size_t defvalue = 0);
		bytes(const bytes& v) : value{ v.value } { ; }
		bytes(const bytes&&) = delete;
		bytes& operator = (const bytes&) = delete;
		bytes& operator = (const bytes&& v) { value = v.value; return *this; }
		inline operator size_t () { return value; }
		inline bool empty() { return !value; }
		template<typename T>
		inline bool operator == (T v) { return (T)value == v; }
		template<typename T>
		inline bool operator != (T v) { return (T)value != v; }
		template<typename T>
		inline bool operator > (T v) { return (T)value > v; }
		template<typename T>
		inline bool operator >= (T v) { return (T)value >= v; }
		template<typename T>
		inline bool operator < (T v) { return (T)value < v; }
		template<typename T>
		inline bool operator <= (T v) { return (T)value <= v; }
	public:
		size_t value;
	};

	class number {
	public:
		number(size_t defvalue = 0) : value{ defvalue } { ; }
		number(const std::string_view&, size_t defvalue = 0);
		number(const number& v) : value{ v.value } { ; }
		number(const number&&) = delete;
		number& operator = (const number& v) { value = v.value; return *this; }
		number& operator = (const number&& v) { value = v.value; return *this; }
		inline operator size_t () { return value; }
		inline bool empty() { return !value; }
		template<typename T>
		inline bool operator == (T v) { return (T)value == v; }
		template<typename T>
		inline bool operator != (T v) { return (T)value != v; }
		template<typename T>
		inline bool operator > (T v) { return (T)value > v; }
		template<typename T>
		inline bool operator >= (T v) { return (T)value >= v; }
		template<typename T>
		inline bool operator < (T v) { return (T)value < v; }
		template<typename T>
		inline bool operator <= (T v) { return (T)value <= v; }
	public:
		size_t value;
	};

	class string {
	public:
		string(const std::string& defvalue = {}) : value{ defvalue } { ; }
		string(const std::string_view& v, const std::string& defvalue = {}, bool trim = true);
		string(const string& v) : value{ v.value } { ; }
		string(const string&&) = delete;
		string& operator = (const string& v) { value = v.value; return *this; }
		string& operator = (const std::string& v) { value = v; return *this; }
		string& operator = (const std::string_view& v) { value = v; return *this; }
		inline bool empty() { return value.empty(); }
		inline bool operator == (const std::string_view& v) { return v.compare(value.data()) == 0; }
		inline bool operator != (const std::string_view& v) { return v.compare(value.data()) != 0; }
	public:
		std::string value;
	};

	class sequence {
	public:
		sequence(const std::string& defvalue = {}, const std::string& delemiter = ",");
		sequence(const std::string_view& v, const std::string& defvalue = {}, const std::string& delemiter = ",") : sequence{ !v.empty() ? std::string{v.begin(),v.end()} : defvalue, delemiter } { ; }
		sequence(const sequence& v);
		sequence(const sequence&&) = delete;
		sequence& operator = (const sequence& v);
		inline bool empty() { return value.empty(); }
		inline auto begin() { return elements.begin(); }
		inline auto end() { return elements.end(); }
		inline bool is(const std::string_view& n) { return elements.find(n) != elements.end(); }
		inline auto size() { return elements.size(); }
		inline std::vector<std::string> list() { return std::vector<std::string>{elements.begin(), elements.end()}; }
	public:
		std::string value;
		std::set<std::string_view> elements;
	};

	class array {
	public:
		array(const std::string& defvalue = {}, const std::string& delemiter = ",");
		array(const std::string_view& v, const std::string& defvalue = {}, const std::string& delemiter = ",") : array{ !v.empty() ? std::string{v.begin(),v.end()} : defvalue, delemiter } { ; }
		array(const coption::array& v);
		array(const coption::array&&) = delete;
		array& operator = (const coption::array&) = delete;
		array& operator = (const coption::array&&) = delete;
		inline bool empty() { return value.empty(); }
		inline auto begin() { return elements.begin(); }
		inline auto end() { return elements.end(); }
		inline auto size() { return elements.size(); }
		inline auto operator [](size_t n) { return (n < elements.size() ? elements[n] : std::string_view{}); }
	public:
		std::string value;
		std::deque<std::string_view> elements;
	};

	class dsn {
	public:
		dsn(const std::string& defvalue = {}){ dsn::operator=(defvalue); }
		dsn(const std::string_view& v, const std::string& defvalue = {}) : dsn{ !v.empty() ? std::string{v.begin(),v.end()} : defvalue } { ; }
		dsn(const dsn& v);
		dsn(const dsn&&) = delete;
		dsn& operator = (const dsn&);
		dsn& operator = (const std::string& v);
		dsn& operator = (const std::string_view& v) { dsn::operator=(std::string{ v.cbegin(), v.cend() }); return *this; }
		inline bool empty() { return value.empty(); }
	public:
		std::string value;
		std::string_view proto, schema, user, password, host, port, path, filename, fullname;
	};

	class pathname {
	public:
		pathname(const std::string& defvalue = {}) { pathname::operator=(defvalue); }
		pathname(const std::string_view& dir, const std::string_view& fname = {},bool resolve = false) : pathname{ } { assign({ dir.cbegin(),dir.cend() }, { fname.cbegin(),fname.cend() }, resolve); }
		//explicit pathname(const std::string_view& v, const std::string& defvalue = {}) : pathname{ !v.empty() ? std::string{v.begin(),v.end()} : defvalue } { ; }
		pathname(const pathname& v);
		pathname(const pathname&&) = delete;
		pathname& operator = (const pathname&);
		pathname& operator = (const std::string& v);
		pathname& operator = (const std::string_view& v) { pathname::operator=(std::string{ v.cbegin(), v.cend() }); return *this; }
		ssize_t assign(const std::string& dir, const std::string& fname, bool resolv = false);
		ssize_t assign(const std::string_view& dir, const std::string_view& fname, bool resolv = false) { return assign({ dir.cbegin(),dir.cend() }, { fname.cbegin(),fname.cend() }, resolv); }
		inline bool empty() { return value.empty(); }
	public:
		std::string value;
		std::string_view path, filename, fullname;
	};

	template<typename ET, typename TP = uint32_t>
	class flag {
		TP flags{ 0 };
		template<typename E, typename T>
		class iterator {
			T flags{ 0 }, mask{ 1 };
			friend class cflags;
			iterator(T fl) : flags{ fl } { ; }
		public:
			inline auto operator ++ () { flags = (TP)(flags >> 1); mask = (TP)(mask << 1); return *this; }
			inline bool operator != (const iterator& w) { return flags != w.flags; }
			inline bool operator == (const iterator& w) { return flags == w.flags; }
			inline E operator *() { return (E)mask; }
		};
	public:
		flag(TP flgs) : flags{ flgs } { ; }
		flag(const std::initializer_list<ET>& flgs = {}) { for (auto& f : flgs) { flags |= (TP)f; } }
		flag(const std::string_view& needle, const std::unordered_map<std::string_view, ET>& haystack_enum_map,const std::string& delimiter = ",") {
			assign(needle, haystack_enum_map, delimiter);
		}
		inline void assign(const std::string_view& needle, const std::unordered_map<std::string_view, ET>& haystack_enum_map, const std::string& delimiter = ",") {
			sequence vallist{ needle,delimiter };
			flags = 0;
			for (auto&& fl : vallist) {
				if (auto it{ haystack_enum_map.find(fl) }; it != haystack_enum_map.end()) {
					flags |= (TP)(it->second);
				}
			}
		}
		flag(const flag& f) : flags{ f.flags } { ; }
		inline void operator |= (ET f) { flags |= (TP)f; }
		inline void operator += (ET f) { flags |= (TP)f; }
		inline void operator &= (ET f) { flags &= (TP)f; }
		inline void operator += (TP f) { flags |= f; }
		inline void operator -= (TP f) { flags &= (~f); }
		inline void operator -= (ET f) { flags &= ~(TP)f; }
		inline void operator = (ET f) { flags = (TP)f;}
		inline bool operator == (ET f) const { return flags == (TP)f; }
		inline void operator = (TP f) { flags = (TP)f; }
		inline bool operator & (ET f) const { return flags & (TP)f; }
		inline bool operator & (TP f) const { return flags & (TP)f; }
		inline TP value()  const{ return flags; }
		inline operator ET() const { return (ET)flags; }

		inline auto begin() const { return flag::iterator<ET, TP>(flags); }
		inline auto end() const { return flag::iterator<ET, TP>(0); }
	};

	template<typename LST = std::unordered_map<std::string, std::string>>
	class props {
		using key_t = typename LST::key_type;
		using value_t = typename LST::value_type;
	public:
		props(const LST& lst) : values{ lst } { ; }
		inline auto at(const key_t& key, const value_t& defvalue = {}) {
			if (auto it = values.find(key); it != values.end()) {
				return it->second;
			}
			return defvalue;
		}
		inline auto begin() const { return values.begin(); }
		inline auto end() const { return values.end(); }
		inline bool empty() { return values.empty(); }
	public:
		LST values;
	};

	class boolean {
	public:
		boolean(bool defvalue = false) : value{ defvalue } { ; }
		boolean(const std::string_view& val, bool defvalue = false) : boolean{ defvalue } { assign(val, defvalue); }
		boolean(const boolean& ) = delete;
		boolean(const boolean&& v) = delete;
		boolean& operator = (const boolean&) = delete;
		boolean& operator = (const boolean&& v) { value = v.value; return *this; }
		inline operator bool () { return value; }
		void assign(const std::string_view& v, bool defvalue = false);
	public:
		bool value;
	};

	static inline std::string one(const std::string_view& needle, const std::string_view& def, const std::unordered_set<std::string_view>& haystack) {
		if (auto&& it{ haystack.find(needle) }; it != haystack.end()) {
			return std::string{ needle };
		}
		return std::string{ def };
	}

	template<typename OPT_T, typename CONTAINER_T>
	static inline OPT_T at(const CONTAINER_T& container, const std::string_view& name) {
		if (auto&& it{ container.find(std::string{name}) }; it != container.end() && !it->second.empty()) {
			return OPT_T{ std::string_view{ it->second } };
		}
		throw std::runtime_error(std::string{ name });
	}

	template<typename OPT_T, typename CONTAINER_T>
	static inline OPT_T optional(const CONTAINER_T& container, const std::string_view& name, const std::string_view& optional_value = {}) {
		if (auto&& it{ container.find(std::string{name}) }; it != container.end() && !it->second.empty()) {
			return OPT_T{ std::string_view{ it->second } };
		}
		return OPT_T{ optional_value };
	}
};
