#include "coption.h"
#include <regex>
#include <list>
#include <climits>

static inline size_t option_value_size_t(const std::string_view& v, size_t def);
static inline std::string_view option_value_trim(const std::string_view& v);
static inline std::deque<std::string_view> option_explode_string(const std::string_view& string, const std::string& delimiter, bool trim);

coption::string::string(const std::string_view& v, const std::string& defvalue, bool trim) {
	if (!v.empty()) { 
		if (trim) {
			auto str{ option_value_trim(v) };
			value.assign(str.begin(), str.end());
		}
		else {
			value.assign(v.begin(), v.end());
		}
	} else { 
		value.assign(defvalue); 
	} 
}

coption::pathname& coption::pathname::operator = (const std::string& v) {
	value.assign(v);
	if (!value.empty()) {
		fullname = std::string_view{ value.data(),value.length() };
		if (auto pos = fullname.rfind('/'); pos != std::string_view::npos) {
			path = fullname.substr(0, pos + 1);
			filename = fullname.substr(pos + 1);
		}
		else {
			filename = fullname;
		}
	}
	return *this;
}

ssize_t coption::pathname::assign(const std::string& dir, const std::string& fname,bool resolv) {
	std::string makepath;
	if (!fname.empty()) {
		if (fname.front() == '/' || fname.front() == '\\') {
			makepath.assign(fname);
		}
		else if(!dir.empty()) {
			makepath.assign(dir);
			if(!(makepath.back() == '/' || makepath.back() == '\\')) makepath.push_back('/');
			makepath.append(fname);
		}
		else {
			makepath.assign(fname);
		}
	}
	else if (!dir.empty()) {
		makepath.assign(dir);
		if (!(makepath.back() == '/' || makepath.back() == '\\')) makepath.push_back('/');
	}
	if (resolv) {
		auto resolved{ (char*)alloca(PATH_MAX) };
		if (::realpath(makepath.c_str(), resolved) != nullptr) {
			pathname::operator=(std::string{ resolved });
			return 0;
		}
		return -errno;
	}
	pathname::operator=(makepath);
	return 0;
}

coption::pathname::pathname(const pathname& v) {
	pathname::operator=(v);
}

coption::pathname& coption::pathname::operator = (const pathname& v) {
	value.assign(v.value);
	fullname = std::string_view{ value.data(),value.length() };
	if (!v.filename.empty()) filename = fullname.substr((size_t)(v.filename.data() - v.value.data()), v.filename.length());
	if (!v.path.empty()) path = fullname.substr((size_t)(v.path.data() - v.value.data()), v.path.length());
	return *this;
}

coption::dsn& coption::dsn::operator = (const std::string& v) {
	static const std::regex re_urn(R"(\s*(([\w-]+)://)(?:(?:([^:@]+)(?::([^@]+))?@)?([^:/$]+)(?::(\d+))?([^\s]*/)?([^\s]*))?)");
	static const std::regex re_host(R"(\s*(?:([^:/]*):(\d+):?)?([^\s]*/)?([^\s]*))");
	//static const std::regex re_opt(R"(([\w-]+)=([^$&]+))");
	value.assign(v);
	std::match_results<std::string_view::const_iterator> match;
	std::string_view str{ value.data(),value.length() };
	if (std::regex_search(str.cbegin(), str.cend(), match, re_urn) && match.size() > 1) {
		schema = std::string_view{ match[1].first, (size_t)match[1].length() };
		proto = std::string_view{ match[2].first, (size_t)match[2].length() };
		user = std::string_view{ match[3].first, (size_t)match[3].length() };
		password = std::string_view{ match[4].first, (size_t)match[4].length() };
		host = std::string_view{ match[5].first, (size_t)match[5].length() };
		port = std::string_view{ match[6].first, (size_t)match[6].length() };
		path = std::string_view{ match[7].first, (size_t)match[7].length() };
		filename = std::string_view{ match[8].first, (size_t)match[8].length() };
		fullname = std::string_view{ match[7].first, (size_t)(match[7].length() + match[8].length()) };
	}
	else if (std::regex_search(str.cbegin(), str.cend(), match, re_host) && match.size() > 1) {
		host = std::string_view{ match[1].first, (size_t)match[1].length() };
		port = std::string_view{ match[2].first, (size_t)match[2].length() };
		fullname = std::string_view{ match[3].first, (size_t)(match[3].length() + match[4].length()) };
		path = std::string_view{ match[3].first, (size_t)match[3].length() };
		filename = std::string_view{ match[4].first, (size_t)match[4].length() };
	}
	return *this;
}

coption::dsn::dsn(const dsn& v) {
	dsn::operator=(v);
}

coption::dsn& coption::dsn::operator = (const coption::dsn& v) {
	value.assign(v.value);
	std::string_view str{ value.data(),value.length() };
	if (!v.proto.empty()) proto = str.substr((size_t)(v.proto.data() - v.value.data()), v.proto.length());
	if (!v.schema.empty()) schema = str.substr((size_t)(v.schema.data() - v.value.data()), v.schema.length());
	if (!v.user.empty()) user = str.substr((size_t)(v.user.data() - v.value.data()), v.user.length());
	if (!v.password.empty()) password = str.substr((size_t)(v.password.data() - v.value.data()), v.password.length());
	if (!v.host.empty()) host = str.substr((size_t)(v.host.data() - v.value.data()), v.host.length());
	if (!v.port.empty()) port = str.substr((size_t)(v.port.data() - v.value.data()), v.port.length());
	if (!v.fullname.empty()) fullname = str.substr((size_t)(v.fullname.data() - v.value.data()), v.fullname.length());
	if (!v.path.empty()) path = str.substr((size_t)(v.path.data() - v.value.data()), v.path.length());
	if (!v.filename.empty()) filename = str.substr((size_t)(v.filename.data() - v.value.data()), v.filename.length());
	return *this;
}

coption::sequence& coption::sequence::operator = (const coption::sequence& v) {
	value.assign(v.value);
	std::string_view str{ value.data(),value.length() };
	for (auto&& el : v.elements) {
		elements.emplace(str.substr((size_t)(el.data() - v.value.data()), el.length()));
	}
	return *this;
}

coption::array::array(const std::string& v, const std::string& delemiter) {
	value.assign(v);
	std::string_view sv{ value.data(),value.length() };
	for (auto&& seq : option_explode_string(value, delemiter, true)) {
		elements.emplace_back(seq);
	}
}

coption::array::array(const coption::array& v) {
	value.assign(v.value);
	for (auto&& el : v.elements) {
		elements.emplace_back(value.data() + (size_t)(el.data() - v.value.data()), el.length());
	}
}

coption::sequence::sequence(const std::string& v, const std::string& delemiter) {
	value.assign(v);
	std::string_view sv{ value.data(),value.length() };
	for (auto&& seq : option_explode_string(value, delemiter, true)) {
		if(!seq.empty()) elements.emplace(seq);
	}
}

coption::sequence::sequence(const sequence& v) {
	value.assign(v.value);
	for (auto&& el : v.elements) {
		elements.emplace(value.data() + (size_t)(el.data() - v.value.data()),el.length());
	}
}

coption::number::number(const std::string_view& v, size_t defvalue) : number{ defvalue } {
	value = option_value_size_t(v, defvalue);
}

coption::bytes::bytes(const std::string_view& val, size_t defvalue) : bytes{ defvalue } {
	static const std::regex re(R"(\s*(\d+)\s*(T|G|M|K|B)?)");
	std::match_results<std::string_view::const_iterator> match;
	if (!val.empty() && std::regex_search(val.cbegin(), val.cend(), match, re) && match.size() > 1) {
		const std::string_view units{ match[2].first, (size_t)match[2].length() };
		value = option_value_size_t({ match[1].first, (size_t)match[1].length() }, 0) <<
			(units == "T" ? 40 : (units == "G" ? 30 : (units == "M" ? 20 : (units == "K" ? 10 : 0))));
	}
}

coption::seconds::seconds(const std::string_view& val, size_t defvalue) : seconds{ defvalue } {
	static const std::regex re(R"((?:\s*(\d+)\s*d)?(?:\s*(\d+)\s*h)?(?:\s*(\d+)\s*m)?(?:\s*(\d+)\s*s{0,1})?)");
	std::match_results<std::string_view::const_iterator> match;
	if (!val.empty() && std::regex_search(val.cbegin(), val.cend(), match, re) && match.size() > 1) {
		value  = option_value_size_t({ match[1].first, (size_t)match[1].length() }, 0) * (24 * 3600);
		value += option_value_size_t({ match[2].first, (size_t)match[2].length() }, 0) * 3600;
		value += option_value_size_t({ match[3].first, (size_t)match[3].length() }, 0) * 60;
		value += option_value_size_t({ match[4].first, (size_t)match[4].length() }, 0);
	}
}

void coption::boolean::assign(const std::string_view& val, bool defvalue) {
	static const std::regex re(R"(\s*(?:true|on|1))", std::regex_constants::icase);
	value = (!val.empty() && std::regex_search(val.cbegin(), val.cend(), re));
 }

static inline std::string_view option_value_trim(const std::string_view& v) {
	auto&& from{ v.begin() }, to{ v.end() };
	for (; std::isspace(*from) && from < to; from++) { ; }
	for (; to > from && std::isspace(*(to - 1)); to--) { ; }
	return { from, static_cast<size_t>(to - from) };
}

static inline size_t option_value_size_t(const std::string_view& v, size_t def) {
	if (!v.empty()) {
		def = 0;
		for (auto ch : v) {
			if (std::isdigit(ch)) {
				def = (def * 10) + (ch - '0');
			}
			else if(!std::isspace(ch)) {
				break;
			}
		}
	}
	return def;
}

static inline std::deque<std::string_view> option_explode_string(const std::string_view& string, const std::string& delimiter, bool trim) {
	/* explode chains */
	std::deque<std::string_view> list;
	std::size_t start = 0;
	std::size_t end = string.find(delimiter);
	if (!string.empty()) {
		if (trim) {
			while (end != std::string_view::npos)
			{
				list.emplace_back(option_value_trim({ string.data() + start, static_cast<size_t>((string.data() + end) - (string.data() + start)) }));
				start = end + delimiter.length();
				end = string.find(delimiter, start);
			}
			list.emplace_back(option_value_trim({ string.data() + start, string.length() - start }));
		}
		else {
			while (end != std::string_view::npos)
			{
				list.emplace_back(string.data() + start, static_cast<size_t>((string.data() + end) - (string.data() + start)));
				start = end + delimiter.length();
				end = string.find(delimiter, start);
			}
			list.emplace_back(string.data() + start, string.length() - start);
		}
	}
	return list;
}