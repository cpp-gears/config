#include "cini.h"
#include <fstream>
#include <regex>

static const std::regex re_config_section(R"(^\s*(\[+)\s*(.*?)\s*\]+)");
static const std::regex re_config_propety(R"(^\s*([\w\.\-:]+)\s*=\s*(?:("|'|)(.*?)\2(.*)))");
static const std::regex re_line_break_clean(R"(^\s*(.*?)\\\s*$)");
static const std::regex re_space_clean(R"(^\s*(.*?)\s*$)");
static const std::regex re_remove_comment(R"(\s*#[^#]*$)");

cini::cini() : head(nullptr) { ; }
cini& cini::operator = (cini&& cfg) { head.swap(cfg.head); return *this; }

inline std::shared_ptr<cini::csection> cini::make_section(std::list<std::string>& path) {
	std::shared_ptr<cini::csection> s_it{ head };
	for (auto&& name : path) {
		if (auto&& sec = s_it->section(name); sec) {
			s_it = sec;
		}
		else {
			auto nsec = s_it->_sections.emplace(name, new cini::csection);
			s_it = nsec.first->second;
		}
	}
	return s_it;
}

cini::cini(const std::string configfile) : head(new csection) {
	std::ifstream cfgfile(configfile);
	if (cfgfile) {
		std::string content;
		std::smatch match;
		std::list<std::string> path;
		size_t line = 0;
		while (std::getline(cfgfile, content) && ++line) {
			if (std::regex_search(content, match, re_line_break_clean) && match.size() > 1) {
				std::string linebreak;
				content = match.str(1);
				while (std::getline(cfgfile, linebreak) && ++line && (std::regex_search(linebreak, match, re_line_break_clean) && match.size() > 1)) {
					content += std::regex_replace(match.str(1), re_remove_comment, "");
				}
				content += std::regex_replace(linebreak, re_space_clean, "$1");
			}
			if (std::regex_search(content, match, re_config_section) && match.size() > 1) {
				size_t clvl = match.str(1).length();
				if (clvl == path.size()) {
					path.pop_back();
					path.emplace_back(std::regex_replace(match.str(2), re_remove_comment, ""));
					make_section(path);
				}
				else if (clvl < path.size()) {
					do {
						path.pop_back();
					} while (path.size() >= clvl);
					path.emplace_back(std::regex_replace(match.str(2), re_remove_comment, ""));
					make_section(path);
				}
				else if (clvl > path.size()) {
					path.emplace_back(std::regex_replace(match.str(2), re_remove_comment, ""));
					make_section(path);
				}
				continue;
			}
			if (std::regex_search(content, match, re_config_propety) && match.size() > 1) {
				if(match.str(2).empty())
					make_section(path)->_properties.emplace(match.str(1), std::regex_replace(match.str(4), re_remove_comment, ""));
				else
					make_section(path)->_properties.emplace(match.str(1), match.str(3));
				continue;
			}
		}
	}
	else {
		throw std::system_error(errno, std::system_category(), configfile);
	}
}
