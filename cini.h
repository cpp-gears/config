#pragma once
#include <string>
#include <unordered_map>
#include <memory>
#include <list>
#include <vector>
#include <string_view>

class cini {
private:
	class csection {
		friend class cini;
		std::unordered_map<std::string, std::shared_ptr<csection>>	_sections;
		std::unordered_map<std::string, std::string>		_properties;
		csection() { ; }
		csection(const csection&& s) : _sections(s._sections), _properties(s._properties) { ; }
	public:
		inline std::unordered_map<std::string, std::shared_ptr<csection>>& sections() { return _sections; }
		inline std::unordered_map<std::string, std::string>& options() { return _properties; }

		inline std::string_view option(const std::string&& name) {
			if (auto&& it{ _properties.find(name) }; it != _properties.end()) {
				return it->second;
			}
			return {};
		}
		inline std::shared_ptr<csection> section(const std::string& name) {
			auto&& it = _sections.find(name);
			return it != _sections.end() ? it->second : std::shared_ptr<csection>{ nullptr };
		}
	};
	std::shared_ptr<csection>	head;
	inline std::shared_ptr<csection> make_section(std::list<std::string>&);
public:
	using options_t = std::unordered_map<std::string, std::string>;
	using sections_t = std::unordered_map<std::string, std::shared_ptr<csection>>;
	using section_t = std::shared_ptr<csection>;

	cini();
	cini(const std::string configfile);
	cini& operator = (cini&& cfg);
	inline cini::section_t section(const std::string&& name) const {
		if (head) {
			auto&& it = head->_sections.find(std::move(name));
			return it != head->_sections.end() ? it->second : std::shared_ptr<csection>{ nullptr };
		}
		return {};
	}
	template<typename ... PATH>
	inline cini::section_t xpath(PATH&& ... path) const {
		if (head) {
			auto section = std::shared_ptr<csection>(head);
			for (auto&& p : std::initializer_list<std::string>({ std::string{ path }... })) {
				if (auto&& it = section->_sections.find(std::move(p)); it != section->_sections.end()) {
					section = it->second;
					continue;
				}
				return {};
			}
			return section;
		}
		return {};
	}
	inline cini::sections_t sections() const {
		if (head) { return head->_sections; }
		return {};
	}
};